<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input[name="rodzajUmowy"]').click(function() {
					if($(this).attr('id') == 'zlecenie') {
						$('.zlecenie').show();
					}
					else {
						$('.zlecenie').hide();
					}
					if($(this).attr('id') == 'dzielo') {
						$('.dzielo').show();
					}
					else {
						$('.dzielo').hide();
					}
				});
			});
		</script>
		<%@page import="java.util.Calendar"%>
		<%@page import="java.util.GregorianCalendar"%>

	<%@ page contentType="text/html; charset=UTF-8" %>
	<title>Zadanie zaliczeniowe 3</title>
	</head>
	<body>
		<h1> Kalkulator płac: </h1>
		 <form action="kalkulator" method="post">
			 <div>Rodzaj umowy: <br>
			 <input type="radio" name="rodzajUmowy" value="praca" checked>Umowa o prace
			 <input type="radio" id="dzielo" name="rodzajUmowy" value="dzielo">Umowa o dzieło<br>
			 <input type="radio" id="zlecenie" name="rodzajUmowy" value="zlecenie">Umowa o zlecenie<br>

			 <%
				 GregorianCalendar cal = new GregorianCalendar();
			 %>
			 Rok:
			  <input type="number" name="rok"  min=1901 value=<%out.print(cal.get(Calendar.YEAR));%> required><br><br>
			  Typ kwoty:
			 <input type="radio" name="typKwoty" value="brutto" checked>Brutto
			 <input type="radio" name="typKwoty" value="netto">Netto<br>
			 Kwota wynagrodzenia:
			  <input type="number" name="kwota" min=1 required><br><br>
				</div>
			 <div class="zlecenie" style="display:none">
				 Składki: <br>
				 Rentowa:
				 <input type="radio" name="rentowa" value="tak" checked>Tak
				 <input type="radio" name="rentowa" value="nie">Nie<br>
				 Emerytalna:
				 <input type="radio" name="emerytalna" value="tak" checked>Tak
				 <input type="radio" name="emerytalna" value="nie">Nie<br>
				 Chrobowa:
				 <input type="radio" name="chorobowa" value="tak" checked>Tak
				 <input type="radio" name="chorobowa" value="nie">Nie<br>
				 Zdrowotna:
				 <input type="radio" name="zdrowotna" value="tak" checked>Tak
				 <input type="radio" name="zdrowotna" value="nie">Nie<br>
				 Koszty uzyskania przychodu:
				 <input type="radio" name="kosztyPrzychodu" value="20" checked>20%
				 <input type="radio" name="kosztyPrzychodu" value="50">50%<br>
			 </div>
			 <div class="dzielo" style="display:none">
				 Koszty uzyskania przychodu:
				 <input type="radio" name="kosztyPrzychodu" value="20" checked>20%
				 <input type="radio" name="kosztyPrzychodu" value="50">50%<br>
		 	</div>
			  <button type="submit">Wylicz</button>
			  <button type="submit" class='btn-export' name='export'>Export to PDF</button>
		</form> 
	</body>



</html>