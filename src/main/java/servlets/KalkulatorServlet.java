package servlets;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Dzielo;
import model.Wynagrodzenie;
import model.SiteSettings;
import model.Zlecenie;

@WebServlet("/kalkulator")
public class KalkulatorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Integer rok = Integer.parseInt(request.getParameter("rok"));
		Double kwota = Double.parseDouble(request.getParameter("kwota"));
		String rodzajUmowy = request.getParameter("rodzajUmowy");
		String typKwoty = request.getParameter("typKwoty");
		Wynagrodzenie wynagrodzenie = new Wynagrodzenie(rok, kwota, typKwoty);

		if (rodzajUmowy.equalsIgnoreCase("dzielo")){
			Double kosztyPrzychodu = Double.parseDouble(request.getParameter("kosztyPrzychodu"));
			Dzielo dzielo = new Dzielo(rok, kwota, typKwoty, kosztyPrzychodu);
		}

		if (rodzajUmowy.equalsIgnoreCase("zlecenie")){
			Double kosztyPrzychodu = Double.parseDouble(request.getParameter("kosztyPrzychodu"));
			String skladkaRentowa = request.getParameter("rentowa");
			String skladkaEmerytalna = request.getParameter("emerytalna");
			String skladkaChorobowa = request.getParameter("chorobowa");
			String skladkaZdrowotna = request.getParameter("zdrowotna");
			Zlecenie zlecenie = new Zlecenie(rok, kwota, typKwoty, kosztyPrzychodu, skladkaRentowa,
					skladkaEmerytalna, skladkaChorobowa, skladkaZdrowotna);

		}

//
//		ArrayList<String> columns = new ArrayList<String>(Arrays.asList(COL1, COL2, COL3, COL4, COL5));
//		SiteSettings site = new SiteSettings();
//		StringBuilder table = createTable(wynagrodzenie, columns);
//		site.setTitle("Zadanie zaliczeniowe 3");
//		site.setHead("<style> th, td {padding: 5px;text-align: left; border: 0.5px solid black;}</style>");
//		site.setBody("<h1>Tabela wyplat</h1>");
//		site.appendToBody(table);
//		response.setContentType("text/html;charset=UTF-8");
//		if (request.getParameter("export") != null) {
//			Reader reader = new StringReader(site.getPage().toString());
//			utils.Helpers.exportToPDF(request, response, reader);
//		} else {
//			response.getWriter().print(site.getPage());
//		}
	}

//	private StringBuilder createTable(Wynagrodzenie wynagrodzenie, ArrayList<String> columns) {
//		double kwota = wynagrodzenie.getKwotaWynagrodzenia();
//		int rok = wynagrodzenie.getRok();
//		String typKwoty = wynagrodzenie.getTypKwoty();
//		String rodzajUmowy = wynagrodzenie.getRodzajUmowy();
//
//		StringBuilder sb = new StringBuilder();
//		sb.append("<table style='border: 1px solid black;'>");
//		sb.append(createRow(columns));
//
//		for (int rateNumber = 1; rateNumber <= 12; rateNumber++) {
//			double amountOfInterest = utils.Helpers.roundTo2Dec(kwota / 12);
//			ArrayList<Number> listOfValues = new ArrayList<Number>(Arrays.<Number>asList(rateNumber, kwota, amountOfInterest,
//					wynagrodzenie.getRate(rateNumber)));
//			sb.append(createRow(listOfValues));
//			kwota -= wynagrodzenie.getRate(rateNumber);
//			kwota = utils.Helpers.roundTo2Dec(kwota);
//		}
//
//		sb.append("</table>");
//		return sb;
//	}

	public <T> String createRow(ArrayList<T> mylist) {
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>");
		for (T value : mylist) {
			sb.append("<td>");
			sb.append(value);
			sb.append("</td>");
		}
		sb.append("</tr>");
		return sb.toString();
	}
}
