package model;

/**
 * Created by saviola on 4/19/16.
 */
public class Dzielo extends Wynagrodzenie{

    private double kosztyPrzychodu;

    public Dzielo(
            int rok,
            double kwotaWynagrodzenia,
            String typKwoty,
            double kosztyPrzychodu) {
        super(rok, kwotaWynagrodzenia, typKwoty);
    }

    public double getKosztyPrzychodu() {
        return kosztyPrzychodu;
    }

    public void setKosztyPrzychodu(double kosztyPrzychodu) {
        this.kosztyPrzychodu = kosztyPrzychodu;
    }

}
